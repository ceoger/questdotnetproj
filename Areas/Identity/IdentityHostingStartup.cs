using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using QuestProject.Areas.Identity.Data;

[assembly: HostingStartup(typeof(QuestProject.Areas.Identity.IdentityHostingStartup))]
namespace QuestProject.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {
                services.AddDbContext<StudentContext>(options =>
                    options.UseNpgsql(
                        context.Configuration.GetConnectionString("StudentContextConnection")));

                services.AddDefaultIdentity<User>()
                    .AddDefaultUI(UIFramework.Bootstrap4)
                    .AddEntityFrameworkStores<StudentContext>();
            });
        }
    }
}