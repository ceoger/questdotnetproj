﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuestProject.Migrations
{
    public partial class studentData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Student",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "FathersName",
                table: "Student",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "Student",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "RollNumber",
                table: "Student",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Country",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "FathersName",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "RollNumber",
                table: "Student");
        }
    }
}
